/*
 * ESPRESSIF MIT License
 *
 * Copyright (c) 2018 <ESPRESSIF SYSTEMS (SHANGHAI) PTE LTD>
 *
 * Permission is hereby granted for use on all ESPRESSIF SYSTEMS products, in which case,
 * it is free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef _WS_STREAM_H_
#define _WS_STREAM_H_

#include "audio_error.h"
#include "audio_element.h"
#include "audio_common.h"

#define APPID 33570571
#define APPKEY  "46CnWyv6al2GGYCakATfmYnF"
#define DEV_PID  15372
#define CONFIG_WEBSOCKET_URI "wss://vop.baidu.com/realtime_asr?sn=esp32"

#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief      WS Stream hook type
 */
typedef enum {
    WS_STREAM_PRE_REQUEST = 0x01, /*!< The event handler will be called before WS Client making the connection to the server */
    WS_STREAM_ON_REQUEST,         /*!< The event handler will be called when WS Client is requesting data,
                                     * If the fucntion return the value (-1: ESP_FAIL), WS Client will be stopped
                                     * If the fucntion return the value > 0, WS Stream will ignore the post_field
                                     * If the fucntion return the value = 0, WS Stream continue send data from post_field (if any)
                                     */
    WS_STREAM_ON_RESPONSE,        /*!< The event handler will be called when WS Client is receiving data
                                     * If the fucntion return the value (-1: ESP_FAIL), WS Client will be stopped
                                     * If the fucntion return the value > 0, WS Stream will ignore the read function
                                     * If the fucntion return the value = 0, WS Stream continue read data from WS Server
                                     */
    WS_STREAM_POST_REQUEST,       /*!< The event handler will be called after WS Client send header and body to the server, before fetching the headers */
    WS_STREAM_FINISH_REQUEST,     /*!< The event handler will be called after WS Client fetch the header and ready to read WS body */
    WS_STREAM_RESOLVE_ALL_TRACKS,
    WS_STREAM_FINISH_TRACK,
    WS_STREAM_FINISH_PLAYLIST,
} ws_stream_event_id_t;

/**
 * @brief      Stream event message
 */
typedef struct {
    ws_stream_event_id_t    event_id;       /*!< Event ID */
    void                    *ws_client;   /*!< Reference to WS Client using by this WS Stream */
    void                    *buffer;        /*!< Reference to Buffer using by the Audio Element */
    int                     buffer_len;     /*!< Length of buffer */
    void                    *user_data;     /*!< User data context, from `ws_stream_cfg_t` */
    audio_element_handle_t  el;             /*!< Audio element context */
} ws_stream_event_msg_t;

typedef int (*ws_stream_event_handle_t)(ws_stream_event_msg_t *msg);


/**
 * @brief   WS Stream configurations, if any entry is zero then the configuration will be set to default values
 */
typedef struct {
    audio_stream_type_t     type;           /*!< Stream type */
    int                     buf_sz;         /*!< Audio Element Buffer size */
    int                     out_rb_size;    /*!< Size of output ringbuffer */
    int                     task_stack;     /*!< Task stack size */
    int                     task_core;      /*!< Task running in core (0 or 1) */
    int                     task_prio;      /*!< Task priority (based on freeRTOS priority) */
    ws_stream_event_handle_t  event_handle;           /*!< The hook function for HTTP Stream */
    void                    *user_data;             /*!< User data context */
    bool                    ext_stack;      /*!< Allocate stack on extern ram */
    bool                    write_header;   /*!< Choose to write amrnb/amrwb header in fatfs whether or not (true or false, true means choose to write amrnb header) */
} ws_stream_cfg_t;


#define WS_STREAM_BUF_SIZE            (4096)
#define WS_STREAM_TASK_STACK          (3072)
#define WS_STREAM_TASK_CORE           (0)
#define WS_STREAM_TASK_PRIO           (4)
#define WS_STREAM_RINGBUFFER_SIZE     (8 * 1024)

#define WS_STREAM_CFG_DEFAULT() {             \
    .type = AUDIO_STREAM_WRITER,                   \
    .buf_sz = WS_STREAM_BUF_SIZE,             \
    .out_rb_size = WS_STREAM_RINGBUFFER_SIZE, \
    .task_stack = WS_STREAM_TASK_STACK,       \
    .task_core = WS_STREAM_TASK_CORE,         \
    .task_prio = WS_STREAM_TASK_PRIO,         \
    .event_handle = NULL,                        \
    .user_data = NULL,                           \
    .ext_stack = false,                          \
}

/**
 * @brief      Create a handle to an Audio Element to stream data from FatFs to another Element
 *             or get data from other elements written to FatFs, depending on the configuration
 *             the stream type, either AUDIO_STREAM_READER or AUDIO_STREAM_WRITER.
 *
 * @param      config  The configuration
 *
 * @return     The Audio Element handle
 */
audio_element_handle_t ws_stream_init(ws_stream_cfg_t *config);

#ifdef __cplusplus
}
#endif

#endif
