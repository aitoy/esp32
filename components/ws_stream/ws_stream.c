/*
 * ESPRESSIF MIT License
 *
 * Copyright (c) 2018 <ESPRESSIF SYSTEMS (SHANGHAI) PTE LTD>
 *
 * Permission is hereby granted for use on all ESPRESSIF SYSTEMS products, in which case,
 * it is free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <sys/unistd.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include "errno.h"

#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "freertos/task.h"

#include "ws_stream.h"
#include "audio_event_iface.h"
#include "esp_websocket_client.h"
#include "esp_event.h"
#include "audio_common.h"
#include "audio_mem.h"
#include "audio_element.h"
#include "wav_head.h"
#include "esp_log.h"
#include "unistd.h"
#include "fcntl.h"
#include <cJSON.h>

#define FILE_WAV_SUFFIX_TYPE  "wav"
#define FILE_OPUS_SUFFIX_TYPE "opus"
#define FILE_AMR_SUFFIX_TYPE "amr"
#define FILE_AMRWB_SUFFIX_TYPE "Wamr"

static const char *TAG = "WS_STREAM";

typedef enum {
    STREAM_TYPE_UNKNOW,
    STREAM_TYPE_WAV,
    STREAM_TYPE_OPUS,
    STREAM_TYPE_AMR,
    STREAM_TYPE_AMRWB,
} wr_stream_type_t;

typedef struct ws_stream {
    audio_stream_type_t type;
    int block_size;
    bool is_open;
    esp_websocket_client_handle_t client;
    ws_stream_event_handle_t hook;
    audio_stream_type_t stream_type;
    void *user_data;
    int  _errno;            /* errno code for ws */
    wr_stream_type_t w_type;
    bool write_header;
} ws_stream_t;


static void log_error_if_nonzero(const char *message, int error_code)
{
    if (error_code != 0) {
        ESP_LOGE(TAG, "Last error %s: 0x%x", message, error_code);
    }
}

static void websocket_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data)
{
    esp_websocket_event_data_t *data = (esp_websocket_event_data_t *)event_data;
    switch (event_id) {
    case WEBSOCKET_EVENT_CONNECTED:
        ESP_LOGI(TAG, "WEBSOCKET_EVENT_CONNECTED");
        esp_websocket_client_handle_t client = (esp_websocket_client_handle_t)data->client;;
        // Define the request payload
        cJSON *req = cJSON_CreateObject();
        cJSON_AddStringToObject(req, "type", "START");
        // char *reqstr = cJSON_PrintUnformatted(req);
        // ESP_LOGI(TAG, "reqstr: %s", reqstr);

        cJSON *data1 = cJSON_CreateObject();
        cJSON_AddNumberToObject(data1, "appid", APPID);
        cJSON_AddStringToObject(data1, "appkey", APPKEY);
        cJSON_AddNumberToObject(data1, "dev_pid", DEV_PID);
        cJSON_AddStringToObject(data1, "cuid", "yourself_defined_user_id");
        cJSON_AddNumberToObject(data1, "sample", 16000);
        cJSON_AddStringToObject(data1, "format", "pcm");
        // char *datastr = cJSON_PrintUnformatted(data1);
        // ESP_LOGI(TAG, "datastr: %s", datastr);
        cJSON_AddItemToObject(req, "data", data1);
        // Convert the JSON payload to a string
        char *body = cJSON_PrintUnformatted(req);
        ESP_LOGI(TAG, "body: %s", body);

        // Send the payload using the WebSocket client
        esp_websocket_client_send_text(client, body, strlen(body), portMAX_DELAY);

        // Cleanup
        cJSON_Delete(req);
        free(body);

        break;
    case WEBSOCKET_EVENT_DISCONNECTED:
        ESP_LOGI(TAG, "WEBSOCKET_EVENT_DISCONNECTED");
        log_error_if_nonzero("HTTP status code",  data->error_handle.esp_ws_handshake_status_code);
        if (data->error_handle.error_type == WEBSOCKET_ERROR_TYPE_TCP_TRANSPORT) {
            log_error_if_nonzero("reported from esp-tls", data->error_handle.esp_tls_last_esp_err);
            log_error_if_nonzero("reported from tls stack", data->error_handle.esp_tls_stack_err);
            log_error_if_nonzero("captured as transport's socket errno",  data->error_handle.esp_transport_sock_errno);
        }
        break;
    case WEBSOCKET_EVENT_DATA:
        ESP_LOGI(TAG, "WEBSOCKET_EVENT_DATA");
        if (data->op_code == 0x08 && data->data_len == 2) {
            ESP_LOGW(TAG, "Received closed message with code=%d", 256 * data->data_ptr[0] + data->data_ptr[1]);
        } else {
            ESP_LOGW(TAG, "Received=%.*s", data->data_len, (char *)data->data_ptr);
        }

        // // If received data contains json structure it succeed to parse
        // cJSON *root = cJSON_Parse(data->data_ptr);
        // if (root) {
        //     for (int i = 0 ; i < cJSON_GetArraySize(root) ; i++) {
        //         cJSON *elem = cJSON_GetArrayItem(root, i);
        //         cJSON *id = cJSON_GetObjectItem(elem, "id");
        //         cJSON *name = cJSON_GetObjectItem(elem, "name");
        //         ESP_LOGW(TAG, "Json={'id': '%s', 'name': '%s'}", id->valuestring, name->valuestring);
        //     }
        //     cJSON_Delete(root);
        // }

        ESP_LOGW(TAG, "Total payload length=%d, data_len=%d, current payload offset=%d\r\n", data->payload_len, data->data_len, data->payload_offset);

        break;
    case WEBSOCKET_EVENT_ERROR:
        ESP_LOGI(TAG, "WEBSOCKET_EVENT_ERROR");
        log_error_if_nonzero("HTTP status code",  data->error_handle.esp_ws_handshake_status_code);
        if (data->error_handle.error_type == WEBSOCKET_ERROR_TYPE_TCP_TRANSPORT) {
            log_error_if_nonzero("reported from esp-tls", data->error_handle.esp_tls_last_esp_err);
            log_error_if_nonzero("reported from tls stack", data->error_handle.esp_tls_stack_err);
            log_error_if_nonzero("captured as transport's socket errno",  data->error_handle.esp_transport_sock_errno);
        }
        break;
    }
}


static wr_stream_type_t get_type(const char *str)
{
    char *relt = strrchr(str, '.');
    if (relt != NULL) {
        relt ++;
        ESP_LOGD(TAG, "result = %s", relt);
        if (strncasecmp(relt, FILE_WAV_SUFFIX_TYPE, 3) == 0) {
            return STREAM_TYPE_WAV;
        } else if (strncasecmp(relt, FILE_OPUS_SUFFIX_TYPE, 4) == 0) {
            return STREAM_TYPE_OPUS;
        } else if (strncasecmp(relt, FILE_AMR_SUFFIX_TYPE, 3) == 0) {
            return STREAM_TYPE_AMR;
        } else if (strncasecmp(relt, FILE_AMRWB_SUFFIX_TYPE, 4) == 0) {
            return STREAM_TYPE_AMRWB;
        } else {
            return STREAM_TYPE_UNKNOW;
        }
    } else {
        return STREAM_TYPE_UNKNOW;
    }
}

static int dispatch_hook(audio_element_handle_t self, ws_stream_event_id_t type, void *buffer, int buffer_len)
{
    ws_stream_t *ws_stream = (ws_stream_t *)audio_element_getdata(self);

    ws_stream_event_msg_t msg;
    msg.event_id = type;
    msg.ws_client = ws_stream->client;
    msg.user_data = ws_stream->user_data;
    msg.buffer = buffer;
    msg.buffer_len = buffer_len;
    msg.el = self;
    if (ws_stream->hook) {
        return ws_stream->hook(&msg);
    }
    return ESP_OK;
}

static esp_err_t _ws_open(audio_element_handle_t self)
{
    ws_stream_t *ws = (ws_stream_t *)audio_element_getdata(self);

    audio_element_info_t info;
    char *uri = audio_element_get_uri(self);
    if (uri == NULL) {
        ESP_LOGE(TAG, "Error, uri is not set");
        return ESP_FAIL;
    }
    ESP_LOGW(TAG, "_ws_open URI=%s", uri);
    audio_element_getinfo(self, &info);

    if (ws->is_open) {
        ESP_LOGE(TAG, "already opened");
        return ESP_FAIL;
    }
  if (ws->client == NULL) {
        esp_websocket_client_config_t websocket_cfg = {};
        websocket_cfg.uri = CONFIG_WEBSOCKET_URI;
        ESP_LOGW(TAG, "_ws_open Connecting to %s...", websocket_cfg.uri);
        ws->client = esp_websocket_client_init(&websocket_cfg);
        AUDIO_MEM_CHECK(TAG, ws->client, return ESP_ERR_NO_MEM);
    } else {
        esp_websocket_client_set_uri(ws->client, uri);
    }

    esp_websocket_register_events(ws->client, WEBSOCKET_EVENT_ANY, websocket_event_handler, (void *)ws->client);
    if (dispatch_hook(self, WS_STREAM_PRE_REQUEST, NULL, 0) != ESP_OK) {
        ESP_LOGE(TAG, "Failed to process user callback");
        return ESP_FAIL;
    }

    ws->is_open = true;
    audio_element_report_codec_fmt(self);
    return ESP_OK;
}

static int _ws_read(audio_element_handle_t self, char *buffer, int len, TickType_t ticks_to_wait, void *context)
{
    ws_stream_t *ws = (ws_stream_t *)audio_element_getdata(self);
    audio_element_info_t info;
    audio_element_getinfo(self, &info);

    ESP_LOGD(TAG, "read len=%d, pos=%d/%d", len, (int)info.byte_pos, (int)info.total_bytes);
    /* use file descriptors to access files */
    ESP_LOGW(TAG, "READ OPERATION SUPPORTING");
    return ESP_OK;
}

static int _ws_write(audio_element_handle_t self, char *buffer, int len, TickType_t ticks_to_wait, void *context)
{
    ws_stream_t *ws = (ws_stream_t *)audio_element_getdata(self);
    audio_element_info_t info;
    audio_element_getinfo(self, &info);
    ESP_LOGW(TAG, "WRITE OPERATION SUPPORTING");
    return ESP_OK;
}

static int _ws_process(audio_element_handle_t self, char *in_buffer, int in_len)
{
    ESP_LOGW(TAG, "_ws_process in_len=%d", in_len);
    int r_size = audio_element_input(self, in_buffer, in_len);
    ESP_LOGW(TAG, "_ws_process r_size=%d", r_size);

    if (audio_element_is_stopping(self) == true) {
        ESP_LOGW(TAG, "No output due to stopping");
        return AEL_IO_ABORT;
    }
    int w_size = 0;
    if (r_size > 0) {
        ws_stream_t *ws = (ws_stream_t *)audio_element_getdata(self);
        w_size = audio_element_output(self, in_buffer, r_size);
    } else {
        w_size = r_size;
    }
    return w_size;
}

static esp_err_t _ws_close(audio_element_handle_t self)
{
    ws_stream_t *ws = (ws_stream_t *)audio_element_getdata(self);

    ESP_LOGW(TAG, "CLOSE OPERATION SUPPORTING");

    return ESP_OK;
}

static esp_err_t _ws_destroy(audio_element_handle_t self)
{
    ws_stream_t *ws = (ws_stream_t *)audio_element_getdata(self);
    ESP_LOGW(TAG, "_ws_destroy");
    audio_free(ws);
    return ESP_OK;
}

// Example of using an audio element - START
audio_element_handle_t ws_stream_init(ws_stream_cfg_t *config)
{
    ESP_LOGW(TAG, "ws_stream_init");
    audio_element_handle_t el;
    ws_stream_t *ws = audio_calloc(1, sizeof(ws_stream_t));

    AUDIO_MEM_CHECK(TAG, ws, return NULL);

    audio_element_cfg_t cfg = DEFAULT_AUDIO_ELEMENT_CONFIG();
    cfg.open = _ws_open;
    cfg.close = _ws_close;
    cfg.process = _ws_process;
    cfg.destroy = _ws_destroy;
    cfg.task_stack = config->task_stack;
    cfg.task_prio = config->task_prio;
    cfg.task_core = config->task_core;
    cfg.out_rb_size = config->out_rb_size;
    cfg.buffer_len = config->buf_sz;
    cfg.stack_in_ext = config->ext_stack;
    if (cfg.buffer_len == 0) {
        cfg.buffer_len = WS_STREAM_BUF_SIZE;
    }

    cfg.tag = "ws";
    ws->hook = config->event_handle;
    ws->stream_type = config->type;
    ws->user_data = config->user_data;

    if (config->type == AUDIO_STREAM_WRITER) {
        cfg.write = _ws_write;
    } else {
        cfg.read = _ws_read;
    }
    el = audio_element_init(&cfg);

    AUDIO_MEM_CHECK(TAG, el, goto _ws_init_exit);
    audio_element_setdata(el, ws);
    return el;
_ws_init_exit:
    audio_free(ws);
    return NULL;
}
// Example of using an audio element - END